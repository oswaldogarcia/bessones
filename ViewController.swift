//
//  ViewController.swift
//  BessonesApp
//
//  Created by Oswaldo Garcia on 3/4/19.
//  Copyright © 2019 Oswaldo Garcia. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var mainWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "https://bessonesturismocolombia.com")!
        mainWebView.load(URLRequest(url: url))
        mainWebView.allowsBackForwardNavigationGestures = true
    }


}

